<?php
class Words extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('words_model');
	}
	
	public function index($id)
	{
		$data['words'] = $this->words_model->getWords($id);
		$data['categoryId']=$id;
		$this->load->view("words/index", $data);
 	}
 	public function create()
 	{
 		$newCategoryName= $this->input->post('newWordTextBox');
 		$id=$this->input->post('categoryId');
 		$lastId=$this->words_model->createWord($newCategoryName,$id);
 		$this->load->helper('url');
 		redirect('words/'.$id);
 	}
 	public function deleteWord($id)
 	{
 		$data['words'] = $this->words_model->deleteWord($id);
		
		$this->load->helper('url');
 		redirect('words/'.$id);
 	}
 	
}
?>