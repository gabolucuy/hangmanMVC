<?php
class Words_Model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}
	
	public function getWords($id)
	{
		$this->db->where('categoryId',$id);
		$query = $this->db->get('word');
		return $query->result_array();
	}
	public function createWord($wordName,$idCategory)
	{
		$data= array(
			'text'=>$wordName,'categoryId'=>$idCategory);
		$this->db->insert('word',$data);
		return $this->db->insert_id();
	}
	public function deleteWord($id)
	{
		$data= array('id'=>$id);
		$this->db->delete('word',$data);
		return true;
	}
}
?>