<!DOCTYPE html>
<?php

?>

<html lang="es-BO">
  <head>
    <title>El ahorcado - Palabras</title>
	<meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/admin.css">
  </head>
  <body>
    
    <h1>Palabras </h1>
		<ul>
		  <?php foreach ($words as $words): ?>
		  <li><a><?= $words["text"] ?></a>  <a href="words/deleteWord/<?= $words["id"] ?>"	>[X]</a>
		  </li> 
		  <?php endforeach; ?>
		  <li>
			<form method="POST" action="/words/create">
			  <input type="text" name="newWordTextBox" maxlength="50" />
			  <input type="submit" name="createNewWordButton" value="Crear" />
			  <input type="hidden" name="categoryId" value="<?=$categoryId ?>"/>
			</form>	
		  </li>
		</ul>
  </body>	
</html>