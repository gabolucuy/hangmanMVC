<!DOCTYPE html>

<html lang="es-BO">
  <head>
    <title>El ahorcado - Palabras</title>
	<meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/admin.css">
  </head>
  <body>
    
    <h1>Palabras <?=	 $id ?></h1>
		<ul>
		  <?php foreach ($words as $words): ?>
		  <li><a href="words.php?id=<?= $words["id"] ?>"><?= $words["name"] ?></a> 
		  </li> 
		  <?php endforeach; ?>
		  <li>
			<form method="POST" action="/words/create">
			  <input type="text" name="newWordTextBox" maxlength="50" />
			  <input type="submit" name="createNewWordButton" value="Crear" />

			</form>	
		  </li>
		</ul>
  </body>	
</html>